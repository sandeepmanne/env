/*jshint globalstrict:true, trailing:false, unused:true, node:true */
"use strict";

var processName = require('./process-name')

exports.create = function(options) {
  var raven = require('raven');
  var path = require('path');
  var fs = require('fs');
  var _ = require('lodash');
  var os = require('os');

  var logger = options.logger;
  var nconf = options.config;

  var release = getRelease();

  function getTags(supplied) {
    try {
      var tags = _.extend({}, {
        host: os.hostname(),
        release: release
      }, supplied);

      var appName = processName.getShortProcessName();
      if (appName) {
        tags.job = appName;
      }
      return tags;
    } catch(e) {
      return supplied;
    }
  }

  function getExtras(err, supplied) {
    try {
      // Sentry doesn't actually do a toString on the error,
      // which can be very useful to diagnosing some types of errors
      return _.extend({}, { errorString: err && err.toString() }, supplied);
    } catch(e) {
      return supplied;
    }
  }

  // Add release information for sentry to help tracking when errors started
  function getRelease() {
    try {
      var assetTag = path.join(path.dirname(require.main.filename), 'ASSET_TAG');

      var stat = fs.statSync(assetTag);
      if (stat && stat.isFile()) {
        var release = fs.readFileSync(assetTag, { encoding: 'utf8' });
        return release;
      }
    } catch(e) {
      return;
    }
  }

  if(!nconf.get('errorReporting:enabled')) {
    return function(err, extra, tags) {
      logger.error('No error reporting is enabled so just logging to logger: ', {
        exception: err,
        meta: getExtras(err, extra),
        tags: getTags(tags)
      });
    };
  }


  var ravenUrl = nconf.get('errorReporting:ravenUrl');
  var client = new raven.Client(ravenUrl, { release: release });

  client.on('error', function(err) {
    logger.error('Unable to log error to sentry:', {
      exception: err
    });
  });


  return function(err, extra, tags, request) {
    try {
      client.captureException(err, {
        // https://docs.sentry.io/clients/node/usage/#raven-node-additional-data
        extra: getExtras(err, extra),
        tags: getTags(tags),
        request: request
      });
    } catch(err) {
      logger.error('Unable to log error to sentry:', {
        exception: err
      });
    }
  };


};
