'use strict';

var Promise = require('bluebird');
var HandlebarsMemoizer = require('./templates/handlebars-memoizer');
var path = require('path');

var LEGACY_MANDRILL_TEMPLATES = 'legacy_mandrill_templates';

function NodeMailerMailer(transporter, options) {
  this.transporter = transporter;
  this.logger = options.logger;
  this.stats = options.stats;
  this.config = options.config;

  this.defaultFromEmailName = options.defaultFromEmailName;
  this.defaultFromEmailAddress = options.defaultFromEmailAddress;

  this.overrideTo = options.overrideTo;

  this.handlebarsCache = new HandlebarsMemoizer();
}

NodeMailerMailer.prototype.send = Promise.method(function(options) {
  var templateName = options.templateName;
  if (!templateName) throw new Error('templateName required');

  return Promise.try(function() {
      if (options.data) return Promise.props(options.data);
      return {};
    })
    .bind(this)
    .then(function(data) {
      return this.applyTemplates(templateName, data);
    })
    .spread(function(html, text) {
      var sendOptions = this.getSendOptions(options, html, text);

      return this.transporter.sendMail(sendOptions);
    })
    .then(function(result) {
      /* Email sent successfully */
      this.logger.verbose('Sent email: ' + templateName + ": " + result.id);
      this.stats.event('email_sent');

      var tracking = options.tracking;
      if(tracking) {
        this.stats.event(tracking.event, tracking.data);
      }

      return result;
    })
    .catch(function(err) {
      /* Email sent failure */
      this.logger.error('Email send failed: ' + err, { exception: err });

      this.stats.event('email_send_failed');
      throw err;
    });
});


NodeMailerMailer.prototype.applyTemplates = Promise.method(function(templateName, data) {
  /*
   * These will get replaced at a later stage with a more flexible scheme
   */
  var templateHtmlFilename = path.join(__dirname, LEGACY_MANDRILL_TEMPLATES, templateName + '_html.hbs');
  var templateTextFilename = path.join(__dirname, LEGACY_MANDRILL_TEMPLATES, templateName + '_text.hbs');

  return Promise.join(
    this.handlebarsCache.get(templateHtmlFilename),
    this.handlebarsCache.get(templateTextFilename),
    function(htmlTemplate, textTemplate) {
      return [
        htmlTemplate(data),
        textTemplate(data)
      ]
    });
});

NodeMailerMailer.prototype.getSendOptions = function(options, html, text) {
  return {
    from: {
      name: options.fromName || this.defaultFromEmailName,
      address: options.fromEmail || this.defaultFromEmailAddress
    },
    to: [this.overrideTo || options.to],
    bcc: options.bcc ? [options.bcc] : undefined,
    subject: options.subject,
    text: text,
    html: html
  };
};

module.exports = NodeMailerMailer;
