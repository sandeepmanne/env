'use strict';

var Promise = require('bluebird');
var mandrillClient = require('mandrill-api/mandrill');

exports.create = function(options) {
  var config = options.config;
  var stats = options.stats;
  var logger = options.logger;

  var mandrillApiKey = config.get('mandrill:apiKey')
  var mandrill = new mandrillClient.Mandrill(mandrillApiKey);

  function sendTemplate(options) {
    return new Promise(function(resolve, reject) {
      mandrill.messages.sendTemplate(options, function(result) {
        var first = result[0];
        if(first.status === 'rejected' || first.status === 'invalid') {
          return reject(new Error('Email provider rejected email: ' + (first.reject_reason || first.status)));
        }

        /* Resolve */
        resolve({
          id: first._id,
          status: first.status
        });

      }, function(err) {
        if(!(err instanceof Error) && err.message) {
          return reject(new Error(err.message));
        }

        reject(err);
      });

    });
  }

  /* Resolve all the values in a hash */
  function resolvePromises(data) {
    if(!data || typeof data !== 'object') {
      return Promise.resolve(data);
    }

    var keys = Object.keys(data);
    var values = keys.map(function(k) { return data[k]; });
    return Promise.all(values)
      .then(function(values) {
        /* Create a copy of the data, with the resolved values */
        return values.reduce(function(memo, value, index) {
          var key = keys[index];
          memo[key] = value;
          return memo;
        }, { });
      });
  }

  function convertHashToMandrillVarsArray(hash) {
    return Object.keys(hash).map(function(key) {
      return { name: key, content: hash[key] };
    });
  }

  /**
   * Send an email and return a promise of
   * { id: XXXX, status: 'queued/sent' }
   *
   * Required parameters:
   *   templateName:
   *   subject:
   *   to: (email address)
   *   data: a hash containing values or promises to be resolved
   */
  return function(options) {
    var templateName = options.templateName;
    if (!templateName) throw new Error('templateName required');

    /* Ensure that all promises are resolved */
    return resolvePromises(options.data)
      .then(function(data) {

        var to = [{ email: options.to, type: 'to' }];

        if(options.bcc) {
          to.push({ email: options.bcc, type: 'bcc' });
        }

        return sendTemplate({
            template_name: templateName,
            template_content: convertHashToMandrillVarsArray(data),
            message: {
              subject:    options.subject,
              from_email: options.fromEmail,
              from_name:  options.fromName,
              to:         to,
              tags:       [options.templateName], // used for A/B testing
              global_merge_vars: convertHashToMandrillVarsArray(data)
            }
          });
      })
      .then(function(result) {
        /* Email sent successfully */
        logger.verbose('Sent email: ' + templateName + ": " + result.id);
        stats.event('email_sent');

        var tracking = options.tracking;
        if(tracking) {
          stats.event(tracking.event, tracking.data);
        }

        return result;
      })
      .catch(function(err) {
        /* Email sent failure */
        logger.error('Email send failed: ' + err, { exception: err });

        stats.event('email_send_failed');
        throw err;
      });
  };

};
