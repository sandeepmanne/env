/*jshint globalstrict:true, trailing:false, unused:true, node:true */
"use strict";

process.on('uncaughtException', function(err) {
  console.log('Caught exception: ' + err);
  process.exit(1);
});

var env = require('../../..').create(__dirname);

var client = env.redis.getClient();
var key = 'test:redis-sentinel-test';

var current = 0;

function error(err) {
  console.error('ERROR: ', err);
  setTimeout(increment, 5);
}

function increment() {
  console.log('>incr');
  client.incr(key, function(err) {
    if(err) return error(err);

    client.get(key, function(err, value) {
      if(err) return error(err);
      value = parseInt(value, 10);

      console.log('> value is ', value);
      if(value === current + 1) {
        current = value;
        setTimeout(increment, 5);
      } else {
        console.log('> value is ', value, ' but current is ', current);
        throw new Error("Inconsistent");
      }
    });
  });
}

client.set(key, 0, function(err) {
  if(err) throw err;
  increment();
});


